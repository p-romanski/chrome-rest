chrome.app.runtime.onLaunched.addListener(function() {

    chrome.sockets.tcp.create({}, function(createInfo) {
        console.log("TCP socket created: ", createInfo);

        var hostname = "developer.chrome.com";
        var path = "/home";
        var socketId = createInfo.socketId;

        chrome.sockets.tcp.setPaused(socketId, true, function() {
            chrome.sockets.tcp.connect(socketId, hostname, 443, function(result) {
                if (result < 0) {
                    console.log("Connection error, code: ", result);
                    return;
                }

                console.log("Connected");
                chrome.sockets.tcp.secure(socketId, null, function(securingResult) {

                    chrome.sockets.tcp.setPaused(socketId, false, function() {
                        console.log("Secure result: ", securingResult);
                        // console.log("Last error:", chrome.runtime.lastError);

                        var requestString = "GET " + path + " HTTP/1.1\r\n" +
                            "Host: " + hostname + "\r\n" +
                            "Connection: close\r\n" +
                            "\r\n";

                        console.log(">>> ", requestString);
                        var requestBuffer = stringToArrayBuffer(requestString);
                        chrome.sockets.tcp.send(socketId, requestBuffer, function(sendInfo) {
                            console.log(">>> Written ", sendInfo);
                        });
                    });
                });
            });
        });
    });

    chrome.sockets.tcp.onReceive.addListener(function(info) {
        console.log("<<< Receiving on socket ", info);
        console.log("<<< Data: ", arrayBufferToString(info.data));
    });
});

function arrayBufferToString(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}

function stringToArrayBuffer(str) {
    var buf = new ArrayBuffer(str.length);
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}
